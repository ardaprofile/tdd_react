export const data = [{
    
    title: "Charming House with woodstove",
    image: "https://images.unsplash.com/photo-1577915589301-09000ae0d072?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    location: "Alexandria, NSW",
    price: 340
   },
   {
    title: "Nice Clean room in brownstone studio",
    image: "https://images.unsplash.com/photo-1577915589301-09000ae0d072?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    location: "Bondi, NSW",
    price: 240
   },
   {
    title: "Bright and Sunny Shared Room in Downtown",
    image: "https://images.unsplash.com/photo-1577915589301-09000ae0d072?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    location: "NewCastle, NSW",
    price: 320
   },
   {
    title: "Cod style house on South Shore",
    image: "https://images.unsplash.com/photo-1577915589301-09000ae0d072?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    location: "RouseHill, NSW",
    price: 320
   },
   {
    title: "Superb duplex apartment in historical center",
    image: "https://images.unsplash.com/photo-1577915589301-09000ae0d072?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    location: "Petersham, NSW",
    price: 300
   },
   {
    title: "Cod style house on South Shore",
    image: "https://images.unsplash.com/photo-1577915589301-09000ae0d072?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    location: "BoxHill, NSW",
    price: 320
   },

]