import { getAllByTestId, getNodeText, render } from '@testing-library/react';
import React from 'react';
import bookingDialogService from '../../services/bookingDialogService';
import Homes from '../Homes';




let container = null;
beforeEach(() => {
  container = render(<Homes />).container;
});

it('should show Homes', () => {
  const homes = getAllByTestId(container, 'home');
  expect(homes.length).toBeGreaterThan(0);
})

it('should show home title', () => {
  const homeTitles = getAllByTestId(container, 'home-title');
  expect(getNodeText(homeTitles[0])).toBe('Charming House with woodstove');
});

it('should show an image', () => {
  const homeImages = getAllByTestId(container, 'home-image');
  expect(homeImages[0]).toBeTruthy();
});

it('should show location', () => {
  const homeLocation = getAllByTestId(container, 'home-location');
  expect(getNodeText(homeLocation[0])).toBe('Alexandria, NSW');
}); 

it('should show Price', () => {
  const homePrice = getAllByTestId(container, 'home-price');
  expect(getNodeText(homePrice[0])).toBe('$340/night');
});


it('should show home Booking button', () => {
  const bookingButton = getAllByTestId(container, 'booking-button');
  expect(bookingButton[0]).toBeTruthy();
});



it('should open home booking dialog when clicking the button', () => {
  jest.spyOn(bookingDialogService, 'open').mockImplementation(() => { });
  const bookingButton = getAllByTestId(container, 'booking-button');
  bookingButton[0].click();
  expect(bookingDialogService.open).toHaveBeenCalledWith({  
    title: "Charming House with woodstove",
    image: "https://images.unsplash.com/photo-1577915589301-09000ae0d072?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    location: "Alexandria, NSW",
    price: 340
   });
});
