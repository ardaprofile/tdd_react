import { getByTestId, render } from '@testing-library/react';
import React from 'react'
import Header from '../Header'


let container = null;

 beforeEach(() => {
 container = render(<Header />).container;
}); 

it('checking existence of logo', () => {
 expect(getByTestId(container, 'logo')).toBeTruthy();
 
});

 it('checking existence of search', () => {
 expect(getByTestId(container, 'search')).toBeTruthy();

});

it('checking existence of menu', () => {
 expect(getByTestId(container, 'menu')).toBeTruthy();
});

it('checking existence of sorting home-type', () => {
 expect(getByTestId(container, 'home-type')).toBeTruthy();
 expect(getByTestId(container, 'dates')).toBeTruthy();
 expect(getByTestId(container, 'guests')).toBeTruthy();
 expect(getByTestId(container, 'price')).toBeTruthy();
 expect(getByTestId(container, 'rooms')).toBeTruthy();
 expect(getByTestId(container, 'amenities')).toBeTruthy();
 


});




