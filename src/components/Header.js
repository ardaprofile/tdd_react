import React from 'react'

function Header() {
 return (
  <div>
  <nav className="py-3 border-bottom navbar navbar-expand navbar-light">
     <a data-testid="logo" href="/#" className="navbar-brand">
     <img alt="logo" src="logo192.png" width="40"/>
    </a>
    <form action="" data-testid="search" className="mr-auto w-50 form-inline">
     <input type="text" placeholder="Search Homes" className="w-50 form-control" />
    </form> 
    <div data-testid="menu" className="ml-auto text-uppercase navbar-nav">
     <a href="#link" className="nav-link">Sign Up</a>
     <a href="#link" className="nav-link">Sign In</a>
    </div>
   </nav>
   <div className="m-0 px-4 py-2 container-fluid mw-100 border-bottom container">
    <button data-testid="home-type" type="button" className="text-nowrap mr-4 py-1 btn btn-outline-secondary">Home type</button>
    <button data-testid="dates" type="button" className="text-nowrap mr-4 py-1 btn btn-outline-secondary">Dates</button>
    <button data-testid="guests" type="button" className="text-nowrap mr-4 py-1 btn btn-outline-secondary">Guests</button>
    <button data-testid="price" type="button" className="text-nowrap mr-4 py-1 btn btn-outline-secondary">Price</button>
    <button data-testid="rooms" type="button" className="text-nowrap mr-4 py-1 btn btn-outline-secondary">Rooms</button>
    <button data-testid="amenities" type="button" className="text-nowrap mr-4 py-1 btn btn-outline-secondary">Amenities</button>
   </div>

  </div>
 )
}

export default Header
