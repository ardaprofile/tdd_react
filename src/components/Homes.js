import { Dialog, DialogContent } from '@material-ui/core';
import React,{ useEffect, useState } from 'react';
import {data} from '../Data'
import bookingDialogService from '../services/bookingDialogService';
import HomeBooking from './HomeBooking';

export default function Homes() {

  const [homesState, setHomesState] = useState([]);
  useEffect(() => {
    setHomesState(data);  
  }, []);
  const [booking, setBooking] = useState({ open: false });

  useEffect(() => {
    const subscription = bookingDialogService.events$.subscribe(state => setBooking(state));
    return () => subscription.unsubscribe();
  }, []);



  let homes;

  homes = homesState.map((home, index) => {
    let { title, image, location, price } = home;
    return (
      <article className="col-6 col-md-6 col-lg-4 col-xl-3 mb-3" data-testid="home" key={index}>
      <img data-testid="home-image" className="card-img-top" src={image} alt={title} />
      <div className="card-body">
      <div data-testid="home-title" className="card w-100">{title}</div>
      <p className="card-title h5"data-testid="home-location">{location}</p>
      <p data-testid="home-price">${price}/night</p>
      <div className="d-flex justify-content-end">
            <button data-testid="booking-button" className="btn btn-info" type="button" onClick={() =>bookingDialogService.open(home)}>Book Now</button>
      </div>
      </div>
    </article>);
  })
  return (
    <div className="container m-2 text-center">
      <h1>Homes</h1>
      <div className="row">{homes}</div>
      <Dialog max-width="xs"open={booking.open} onClose={()=>bookingDialogService.close()}>
        <DialogContent>
          <HomeBooking home={booking.home}/>
        </DialogContent>
      </Dialog>
    </div>
  )
}




